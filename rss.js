function contentLoader()
{
  // Remove previous divisions and clear screen
  var par = document.getElementById("outputArea");   
  while ( par.firstChild )
    par.removeChild( par.firstChild );
 
  var thumbCount = 0;
  var xmlhttp;
  var URL;
  var choice=document.getElementById("selected").selectedIndex;

  var thumbRow = document.createElement("div");
  thumbRow.className = "row";
  
  if(choice == 1)
    URL="http://dev-mystique.tavisca.com/api/deals/all?token=1uo25dyiv2l23lky22fvfwgn&$filter=Type eq 'car'";
  else if(choice == 2)
    URL="http://dev-mystique.tavisca.com/api/deals/all?token=1uo25dyiv2l23lky22fvfwgn&$filter=Type eq 'hotel'";
  else if(choice == 3)
    URL="http://dev-mystique.tavisca.com/api/deals/all?token=1uo25dyiv2l23lky22fvfwgn&$filter=Type eq 'activity'";

  xmlhttp=new XMLHttpRequest();
  xmlhttp.onreadystatechange=function()
  {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
      var result =JSON.parse(xmlhttp.responseText);
      switch(choice){

        case 1:
        var count = result.deals.length;
        document.getElementById('outputArea').appendChild(thumbRow);
        
        for( counter = 0; counter<count; counter++)
        {
          var cars = result.deals[counter];

          if(thumbCount >=  3 )
          {
            thumbRow = document.createElement("div");
            thumbRow.className = "row";
            document.getElementById('outputArea').appendChild(thumbRow);
            thumbCount = 0;
          }

          var col = document.createElement("div");
          col.className = "col-sm-6"+" "+ "col-md-4" ;

          var thumb =  document.createElement("div");
          thumb.className="thumbnail" + " " + "borderDivision";

          col.appendChild(thumb);

          var caption = document.createElement("div");
          caption.className = "caption";

          var heading = document.createElement("H3");
          heading.appendChild(document.createTextNode(cars.rentalCompanyName));
          caption.appendChild(heading);

          var desc = document.createElement("p");
          desc.appendChild(document.createTextNode(cars.description));
          desc.appendChild(document.createTextNode(cars.validityPeriod.start.date + " to " + cars.validityPeriod.end.date));
          desc.appendChild(document.createTextNode("Start time : " + cars.validityPeriod.start.time));
          caption.appendChild(desc);

          thumb.appendChild(caption);
          thumbRow.appendChild(col);

          thumbCount++;
         } 


          /*

          var cardiv = document.createElement("div");          
          cardiv.id = "cdiv"+counter;
          cardiv.className='borderDivision';



          var cardesc = document.createElement("div");
          cardesc.className='description';
          cardiv.appendChild(cardesc);

          var cnamelabel = document.createElement("H1");
          cnamelabel.className='colorLabel';
          var t=document.createTextNode(cars.rentalCompanyName);
          cnamelabel.appendChild(t);
          cardesc.appendChild(cnamelabel);

          var cdesc = document.createElement("H2");
          cdesc.appendChild(document.createTextNode(cars.description));
          cdesc.className = "white-color";
          cardesc.appendChild(cdesc);

          var validityperoid = document.createElement("H3");
          validityperoid.className = "white-color";
          validityperoid.appendChild(document.createTextNode("Validity"));
          cardesc.appendChild(validityperoid);

          var validity = document.createElement("H4");
          validity.className = "white-color";
          t = document.createTextNode(cars.validityPeriod.start.date + " to " + cars.validityPeriod.end.date);
          validity.appendChild(t);
          cardesc.appendChild(validity);

          var ctime = document.createElement("H4");
          ctime.className = "white-color";
          t = document.createTextNode("Start time : " + cars.validityPeriod.start.time);
          ctime.appendChild(t);
          validity.appendChild(ctime);

          document.getElementById("outputArea").appendChild(cardiv);*/

        // }
        break;

        case 2:
        var count = result.deals.length;

        document.getElementById('outputArea').appendChild(thumbRow);

        for( counter = 0;counter<count;counter++)
        {
          var hotels = result.deals[counter];

          if(thumbCount >=  3 )
          {
            thumbRow = document.createElement("div");
            thumbRow.className = "row";
            document.getElementById('outputArea').appendChild(thumbRow);
            thumbCount = 0;
          }

          var col = document.createElement("div");
          col.className = "col-sm-6"+" "+ "col-md-4" ;

          var thumb =  document.createElement("div");
          thumb.className="thumbnail" + " " + "borderDivision";

          var img =  document.createElement("img");
          img.src = hotels.imageUrl;
          thumb.appendChild(img);

          col.appendChild(thumb);

          var caption = document.createElement("div");
          caption.className = "caption";

          var heading = document.createElement("H3");
          heading.appendChild(document.createTextNode(hotels.title));
          caption.appendChild(heading);

          var desc = document.createElement("p");
          desc.appendChild(document.createTextNode(hotels.description));
          caption.appendChild(desc);

          thumb.appendChild(caption);
          thumbRow.appendChild(col);

          thumbCount++;
        }
        break;
      }
    }
  }

  xmlhttp.open("GET",URL,true);
  xmlhttp.send();
}



var e = document.getElementById("selected");

e.onchange = function (){
  contentLoader();
}

function displayCarousel(counter,hotels,carouseldiv){
  var anchor=document.createElement('a');
  anchor.href="#hdiv"+counter;
  anchor.title = hotels.title;
  var hotelimg = document.createElement("img");
  hotelimg.src = hotels.imageUrl;
  hotelimg.className = "hotelimg";
  anchor.appendChild(hotelimg);
  carouseldiv.appendChild(anchor);
  document.getElementById("outputArea").appendChild(carouseldiv);
}